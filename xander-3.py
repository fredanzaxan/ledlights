from flask import Flask, request, render_template
import time
from neopixel import *
import argparse
import random
import flask

# LED strip configuration:
LED_COUNT = 600  # Number of LED pixels.
LED_PIN = 18  # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10  # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 128  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False  # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0  # set to '1' for GPIOs 13, 19, 41, 45 or 53

app = Flask(__name__)
currentColor = '#0000a0'
timer = None
off = False
format = '%H:%m %p'


@app.route('/', methods=['GET', 'POST'])
def index():
    global currentColor
    global off
    global timer
    global strip
    print("This is the code request")
    if request.method == 'POST':
        print("This is a successful post")
        #if request.form.has_key('theaterChase'):
        print(request.args)
        print(request.form)
        if 'theaterChase' in request.form:
            print('Theater Chase')
            theaterChaseRainbow(strip)
        elif 'colorReturn' in request.form:    
            print('colorReturn')
            h = currentColor.lstrip('#')
            print('RGB =', tuple(int(h[i:i + 2], 16) for i in (0, 2, 4)))
            val = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))
            colorReturn(strip, Color(val[1], val[0], val[2]))
        elif 'off' in request.form: 
            print('Starting the off function')
            off = True
            time.sleep(1)
            for i in range(0, LED_COUNT, 1):
                strip.setPixelColorRGB(i, 0, 0, 0)
            strip.show()
            off = False
            print('End the off function')
        elif 'rainbowCycle' in request.form: 
            print('Rainbow Cycle')
            rainbowCycle(strip)
        elif 'middleFill' in request.form: 
            print('MFill')
            middleFill(strip)            
        elif 'breathe' in request.form: 
            print('Breathe')
            breathe(strip)            
        elif 'pattern' in request.form: 
            print('Pattern')
            pattern(strip)
        elif 'random' in request.form: 
            print('random')
            h = currentColor.lstrip('#')
            print('RGB =', tuple(int(h[i:i + 2], 16) for i in (0, 2, 4)))
            val = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))
            randomFill(strip, Color(val[1], val[0], val[2]))     
        elif 'sequenceFill' in request.form: 
            print('sequenceFill')
            sequenceFill(strip)
        elif 'strobe' in request.form: 
            print('strobe')
            h = currentColor.lstrip('#')
            print('RGB =', tuple(int(h[i:i + 2], 16) for i in (0, 2, 4)))
            val = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))
            strobe(strip, Color(val[1], val[0], val[2]))
        elif 'rwb' in request.form: 
            print('RaWB')
            rwb(strip)
        elif 'rgb_color' in request.form: 
            print(request.form['rgb_color'])
            colorInput = request.form['rgb_color']
            currentColor = colorInput.upper()
            print(currentColor)
            h = currentColor.lstrip('#')
            print('RGB =', tuple(int(h[i:i + 2], 16) for i in (0, 2, 4)))
            val = tuple(int(h[i:i + 2], 16) for i in (0, 2, 4))
            for i in range(0, LED_COUNT, 1):
                # for i in range(0,45,1):b
                strip.setPixelColorRGB(i, val[1], val[0], val[2])
            strip.show()
        elif 'slider' in request.form: 
            print('The brightness has been adjusted')
            strip.setBrightness(int(request.form['slider']))
            strip.show()
        else:
            print('CONTACT XANDER SOMETHING HAS BEEN BROKEN')
            return "Something has broken contact Xander Please"
    elif request.method == 'GET':
        # return render_template("index_old.html")
        print("No Post Back Call")
    print('end of index')
    return render_template('index_old.html', color=currentColor,brightness=strip.getBrightness())


    
    
def colorReturn(strip, color):
    print('starting color return')
    for i in range(0,strip.numPixels(),1):
        print('loop',i)
        strip.setPixelColor(i,color)
        for x in range(i+1,strip.numPixels(),1):
            #time.sleep(.01)
            strip.setPixelColor(x,color)
            strip.show()
            strip.setPixelColorRGB(x,0,0,0)
            strip.show()
            time.sleep(0.00001)
            if off:
                return
        for x in range(strip.numPixels(),i+1,-1):
            #time.sleep(.01)
            strip.setPixelColor(x,color)
            strip.show()            
            strip.setPixelColorRGB(x,0,0,0)
            strip.show()
            time.sleep(0.00001)
            if off:
                return
        if off:
            return


def rwb(strip):
    """Murica"""
    for i in range(0, strip.numPixels(), 1):
        if (i % 3 == 0):
            strip.setPixelColorRGB(i, 20, 229, 0)
        elif (i % 3 == 1):
            strip.setPixelColorRGB(i, 255, 128, 255)
        elif (i % 3 == 2):
            strip.setPixelColorRGB(i, 0, 0, 160)
    strip.show()
    
def randomFill(strip, color):
    for i in range(0,strip.numPixels(),1):
        strip.setPixelColorRGB(i,0,0,0)
    numbers = list(range(strip.numPixels()))    
    for x in range(0,strip.numPixels(),1):
        choice = random.choice(numbers)
        numbers.remove(choice)    
        strip.setPixelColor(choice,color)
        strip.show()
        time.sleep(.1)
        if off:
            return
            
def breathe(strip):
    while off != True:
        for i in range(10,255,1):
            strip.setBrightness(i)
            strip.show()
            time.sleep(.01)
            if off: 
                return
        for i in range(255,10,-1):
            strip.setBrightness(i)
            strip.show()
            time.sleep(.01)
            if off: 
                return
def middleFill(strip):
    middle = strip.numPixels()/2;
    colorList=[Color(255,0,0),Color(0,255,0),Color(0,0,255),Color(255,255,0),Color(255,0,255),Color(0,255,255),Color(0,128,128),Color(0,128,0),Color(192,192,192)]
    for i in range(0,strip.numPixels(),1):
        strip.setPixelColorRGB(i,0,0,0)
    while off != True:
        choice = random.choice(colorList)
        for x in range(0,int(middle),1):
            strip.setPixelColor(int(middle)+x,choice)
            strip.setPixelColor(int(middle)-x,choice)
            strip.show()
            time.sleep(.00001)
            if off:
                return


        
def strobe(strip, color):
    """Cancer between a few colors"""
    for x in range(0, strip.numPixels(), 1):
        for i in range(0, strip.numPixels(), 1):
            strip.setPixelColor(i, Color(255, 0, 0))
        strip.show()
        time.sleep(.1)
        for i in range(0, strip.numPixels(), 1):
            strip.setPixelColor(i, color)
        strip.show()
        time.sleep(.1)
        if off:
            return
def sequenceFill(strip):
    colorList=[Color(255,0,0),Color(0,255,0),Color(0,0,255),Color(255,255,0),Color(255,0,255),Color(0,255,255),Color(0,128,128),Color(0,128,0),Color(192,192,192)]
    for i in range(0,strip.numPixels(),1):
        strip.setPixelColorRGB(i,0,0,0)
        
    for i in range(strip.numPixels(),0,-1):
        choice = random.choice(colorList)
        for x in range(0,i,1):
            strip.setPixelColor(x,choice)
            strip.show()
            if x != (i-1):
                strip.setPixelColorRGB(x,0,0,0)            
                strip.show()
            time.sleep(0.00001)
            if off:
                return
                
def pattern(strip):
    """Switches between two colors"""
    print('Pattern on')
    for i in range(0, strip.numPixels(), 1):
        if i % 2 == 0:
             strip.setPixelColor(i, Color(0, 100, 0))
             strip.show()
        else:
             strip.setPixelColor(i, Color(255, 0, 0))
             strip.show()


def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms / 1000.0)


def theaterChase(strip, color, wait_ms=50, iterations=10):
    """Movie theater light style chaser animation."""
    for j in range(iterations):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, color)
            strip.show()
            time.sleep(wait_ms / 1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, 0)
        if off:
            print('Breaking the theater chase')
            return


def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)


def rainbow(strip, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256 * iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i + j) & 255))
        strip.show()
        time.sleep(wait_ms / 1000.0)


def rainbowCycle(strip, wait_ms=20, iterations=99999):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256 * iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((int(i * 256 / strip.numPixels()) + j) & 255))
        strip.show()
        time.sleep(wait_ms / 1000.0)
        if off:
            return


def theaterChaseRainbow(strip, wait_ms=50):
    """Rainbow movie theater light style chaser animation."""
    for j in range(256):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, wheel((i + j) % 255))
            strip.show()
            time.sleep(wait_ms / 1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, 0)
        if off:
            return
           
    
if __name__ == '__main__':
    # Process arguments
    print(flask.__version__)
    print('THIS IS THE MAIN FUNCITON ')
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()
    global strip
    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()
    print('Starting the blue color')
    for i in range(0, 30, 1):
        strip.setPixelColorRGB(i, 0, 0, 0)
    strip.show()
    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')
    #app.run(port='80', debug=True, host='0.0.0.0')
    #app.run('0.0.0.0',80,True, threaded=True)

    app.run(port=80, debug=True, host='0.0.0.0', threaded=True)

